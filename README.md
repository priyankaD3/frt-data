# __Lemmatized Marathi data (FRT)__ ![v.1](https://img.shields.io/badge/Version-1-success)

This data set has been created as a part of
M. Phil. research work submitted to the department of
linguistics, University of Mumbai in 2021.

## Description of the data set

To create this data set, written language samples were
collected through a Film Retelling Task (FRT) with 24
participants (having Marathi as their first language). Then
these language samples were manually lemmatized.

The 24 participants were equally divided into two groups:

  1. The Marathi Medium Group which comprised of
	 participants who studied in Marathi medium schools,
  2. The English medium Group which comprised of
	 participants who studied in English medium schools. 
	 
The characteristics of the participants are mentioned in
the following table.

||Marathi Medium Group|English Medium Group|
|---|---|---|
|Total number of participants|12|12|
|Number of girls|6|2|
|Number of boys|6|10|
|Mean age of the participants|15.25|14.75|
|Standard wise distribution of the participants |8th = 1<br>9th = 3<br>10th = 1<br>11th = 3<br>1st year of graduation = 3<br>3rd year of graduation = 1 |7th = 1<br>8th = 3<br>9th = 1<br>11th = 3<br>12th = 2<br>1st year of graduation = 1<br>2nd year of graduation = 1|

Each of these 24 participants saw and described film
excerpts from two movies (Modern Times and Pushpak
Vimana). Neither of these films have any verbal dialogue. A
9-minute excerpt from the Modern Times movie (from minute
19:51 to minutes 29:09) was used. The film is available at
https://www.youtube.com/watch?v=2gLa4wAia9g. A 13-minute
excerpt from the Pushpaka Vimana movie (from minute 5.47 to
minute 19.43) was used. This movie is available at
https://www.youtube.com/watch?v=EvvagYtOJX8&t=461s.

The handwritten language samples collected from the
participants were converted into text files using a text
processor and then three operations were performed on them
namely _Correction of spelling and grammatical mistakes_,
_Removal of proper nouns and borrowed words_ and
_Lemmatization_.

## How to read a file

### How to read a file name

The file name contains the following information:

  1. Participant's medium of instruction in school (__E__
	 stands for English medium and __M__ stands for Marathi
	 medium)
  2. Name of the movie excerpt (__CC__ stands for Charlie
	 Chaplin's  Modern Times movie and __P__ stands for
	 Pushpaka Vimana movie)
  3. Participant's unique number (__01__ stands for the
     first participant, __02__ stands for second
     participants, so on and so forth.)
  
e.g., File name __EP11__ is of a participant number __11__
who belongs to the English medium group and the language
sample is form Pushpaka Vimana movie.

### How to read the content of the file

In each file, personal details of the participants such as
age, sex, standard have been mentioned. Besides, the file
contains following four texts:

T1: original text (i.e. handwritten language sample
converted into a typed-text)

T2: text obtained after correction of spelling and
grammatical mistakes in T1

T3: text obtained after removal of proper nouns and borrowed
words from T2

T4: text obtained after lemmatization of T3

## Data Formats

__Lemmatized Marathi data (FRT)__ data set contains total 48
files in three formats i.e., Plain text format (.txt),
Microsoft word format (.docx), OpenDocument Text File format
(.odt).

## Acknowledgements

I would like to thank Jueli Adesh Mhatre for proofreading
the entire data set and Sayali Sunil Pawar for checking the
faithfulness of the typed text. I would also like to thank
[Niranjan](https://gitlab.com/niruvt) for helping me to decide the license for this work
and setting up this repository.

## License

© 2021 Priyanka Dingankar

This work is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International
License</a>.<br><br>
<a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License"
style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a>
